/*

    Author: tomasz.miller@outlook.com

    https://jsfiddle.net/user/peniakoff/fiddles/

*/

const TheGrid = function (elementsParentID, tileMainClass, smallTileClass, largeTileClass) {

  this.numberOfColumns = 3;
  this.latency = 25;
  this.functionIsActive = false;
  this.classNames = [];
  this.elementsParentID = elementsParentID;
  this.tileMainClass = tileMainClass;
  this.smallTileClass = smallTileClass;
  this.largeTileClass = largeTileClass;

  (this.saveAllClassNames = () => {
    let elementsParent = $('#' + this.elementsParentID)[0],
      tilesElements = elementsParent.getElementsByClassName(this.tileMainClass),
      tilesElementsLength = tilesElements.length,
      i = 0;
    for (i; i < tilesElementsLength; i++) {
      this.classNames[i] = tilesElements[i].className;
    }
  })();

  this.showElement = (element, index, j) => {
    element.style.display = "flex";
    if (index === j) {
      this.setExtraClassName(element);
    } else {
      this.setNewClassName(element);
    }
  }

  this.hideElement = (element, index) => {
    element.className = this.classNames[index];
    element.style.display = "none";
  }

  this.setNewClassName = element => {
    element.className += " " + this.smallTileClass;
  }

  this.setExtraClassName = element => {
    element.className += " " + this.largeTileClass;
  }

  this.showElements = elementClassName => {
    let elementsParent = $('#' + this.elementsParentID)[0],
      tilesElements = elementsParent.getElementsByClassName(elementClassName),
      tilesElementsLength = tilesElements.length,
      i = 0,
      j = 0,
      k = 1,
      runThisFunction = () => {
        this.showElement(tilesElements[i], i, j);
        if (i === j) {
          if (k === 1) {
            k = 2 * this.numberOfColumns;
          } else if (k === 2 * this.numberOfColumns) {
            k = 1;
          }
          j += k;
        }
        i++;
        if (i === tilesElementsLength) {
          this.functionIsActive = false;
        }
        if (i < tilesElementsLength) {
          setTimeout(runThisFunction, this.latency);
        }
      };
    if (this.functionIsActive === true) return;
    this.hideAll();
    this.functionIsActive = true;
    runThisFunction();
  }

  this.showAll = () => {
    this.showElements(this.tileMainClass);
  }

  this.hideAll = () => {
    let elementsParent = $('#' + this.elementsParentID)[0],
      tilesElements = elementsParent.getElementsByClassName(this.tileMainClass),
      tilesElementsLength = tilesElements.length,
      i = tilesElementsLength - 1,
      j = i,
      runThisFunction = () => {
        this.hideElement(tilesElements[i], j);
        i--;
        j--;
        if (i === 0) {
          this.functionIsActive = false;
        }
        if (i >= 0) {
          runThisFunction();
        }
      };
    if (this.functionIsActive === true) return;
    this.functionIsActive = true;
    runThisFunction();
  }

  this.getClassName = className => {
    return className.replace(/ /g, ".");
  }

}

const TILES_GRID_1 = new TheGrid('project-list', 'images-wrapper__item', 'images-wrapper__item--small', 'images-wrapper__item--large');
TILES_GRID_1.showAll();

//const TILES_GRID_2 = new TheGrid('project-list-2', 'images-wrapperPk__item', 'images-wrapperPk__item--small', 'images-wrapperPk__item--large');
//TILES_GRID_2.showAll();