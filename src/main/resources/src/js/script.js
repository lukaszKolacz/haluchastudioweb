//fixed navigation on scroll
document.onscroll = (function (event) {
    var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    console.log(scrollTop);
   if ($(window).scrollTop() > 200) {
    if(!$('.header').hasClass('fixed') && !$('.header > .nav-block').hasClass('nav-block--transp') && !$('.project__description').hasClass('project__description--fixed') && !$('.project__grafics').hasClass('project__grafics--scrolled')){
         $('.header').addClass('fixed');
         $('.header >  .nav-block').addClass('nav-block--transp');
         $('.project__description').addClass('project__description--fixed');
         $('.project__grafics').addClass('project__grafics--scrolled');
    }
    }
    else {
        if($('.header').hasClass('fixed') && $('.header > .nav-block').hasClass('nav-block--transp') && $('.project__description').hasClass('project__description--fixed') && $('.project__grafics').hasClass('project__grafics--scrolled')){
            $('.header').removeClass('fixed');
            $('.header > .nav-block').removeClass('nav-block--transp');
            $('.project__description').removeClass('project__description--fixed');
            $('.project__grafics').removeClass('project__grafics--scrolled');
        }
    }

});


//smooth clicking between links
$('a[href^="#"]').click(function(){
var target = $(this).attr('href');
$('html, body').animate({scrollTop: $(target).offset().top - 75}, 800);
return false;
});

//single project back btn
$("#backLink").click(function(event){
    event.preventDefault();
    $("body").fadeOut(100, redirectPage);
});

function redirectPage() {
    history.back(1);
}


//Portfolio navigation
$("#commercial-proj").click(function(){
    $(".images-wrapper__item--commercial").show();
    $('.projects-filter--secondary').hide();
    $(".images-wrapper__item--private").hide();
    $(".images-wrapper__item--public").hide();
});
$("#private-proj").click(function(){
    $('.projects-filter--secondary').css('display', 'flex');
    $(".images-wrapper__item--private").show();
    $(".images-wrapper__item--commercial").hide();
    $(".images-wrapper__item--public").hide();
});
$("#public-proj").click(function(){
    $('.projects-filter--secondary').hide();
    $(".images-wrapper__item--public").show();
    $(".images-wrapper__item--commercial").hide();
    $(".images-wrapper__item--private").hide();
});
$("#all-proj").click(function(){
    $(".images-wrapper__item--public").show();
    $(".images-wrapper__item--commercial").show();
    $(".images-wrapper__item--private").show();
    $(".projects-filter--secondary").hide();
});


/*Filtering by project size*/
$('#small-proj').click(function(){
  $(".images-wrapper__proj--small").show();
  $(".images-wrapper__proj--mid").hide();
  $(".images-wrapper__proj--big").hide();
});

$('#mid-proj').click(function(){
  $(".images-wrapper__proj--mid").show();
  $(".images-wrapper__proj--small").hide();
  $(".images-wrapper__proj--big").hide();
});

$('#big-proj').click(function(){
  $(".images-wrapper__proj--big").show();
  $(".images-wrapper__proj--mid").hide();
  $(".images-wrapper__proj--small").hide();
});

//Portfolio navigation change active item
$(".projects-filter .projects-filter__btn").click(function(){
    $(this).addClass("projects-filter__btn--active");
    $(".projects-filter .projects-filter__btn").not(this).removeClass("projects-filter__btn--active");
});
$(".projects-filter--secondary .projects-filter__sub-btn").click(function(){
    $(this).addClass("projects-filter__sub-btn--active");
    $(".projects-filter--secondary .projects-filter__sub-btn").not(this).removeClass("projects-filter__sub-btn--active");
});

$('.secondary-nav').hide();
//MAIN NAVIGATION SWITCH
$(".primary-nav li .primary-nav__link").click(function(){
    $(this).addClass("primary-nav__link--active");
    $(".secondary-nav li .secondary-nav__link:first").addClass("secondary-nav__link--active");
    $(".primary-nav .primary-nav__link").not(this).removeClass("primary-nav__link--active");
    $(this).parent().children("ul.secondary-nav").show();
    $(this).not( ".primary-nav__sub-link" ).each(function(){
      $("ul.secondary-nav").hide();
    });
});




/*Slider croll btn*/
$("#scroll-btn").click(function() {
    $('html,body').animate({
        scrollTop: $("#about").offset().top - 75},
        'slow');
}); 


//'Oferta' switch between data
$("#offer-projects").click(function(){
  if (!$(this).hasClass('offer__item--active')) {
    $(this).addClass('offer__item--active');
    $('#offer-planning').removeClass('offer__item--active');
    $('#offer-education').removeClass('offer__item--active');
  }
  $("#offer-projects-content").show(1000);
  $("#offer-planning-content").hide(50);
  $("#offer-education-content").hide(50);
});

$("#offer-planning").click(function(){
  if (!$(this).hasClass('offer__item--active')) {
    $(this).addClass('offer__item--active');
    $('#offer-projects').removeClass('offer__item--active');
    $('#offer-education').removeClass('offer__item--active');
  }
  $("#offer-planning-content").show(1000);
  $("#offer-projects-content").hide(50);
  $("#offer-education-content").hide(50);
});

$("#offer-education").click(function(){
  if (!$(this).hasClass('offer__item--active')) {
    $(this).addClass('offer__item--active');
    $('#offer-projects').removeClass('offer__item--active');
    $('#offer-planning').removeClass('offer__item--active');
  }
  $("#offer-education-content").show(1000);
  $("#offer-planning-content").hide(50);
  $("#offer-projects-content").hide(50);
});


//--------switch to single project
$(".project-link").click(function () {
  $('.projects--single').show('slow');
  $('.projects--single').css('display', 'block');
  $('.close-btn').click(function () {
    $('.projects--single').hide('slow');
    $('.projects--single').css('display', 'none');
  });
});




//NAVIGATION

var navLinks= document.querySelectorAll('.secondary-nav__link');

$(".secondary-nav li .secondary-nav__link").click(function(){
    $(this).addClass("secondary-nav__link--active");
    $(".secondary-nav li .secondary-nav__link").not(this).removeClass("secondary-nav__link--active");
});



$("#mobNavBtn").click(function(e) {
  e.preventDefault();
  $('.nav-block--mob').toggleClass('nav-block--open', 2000);
  $('.nav-block--mob li a').on('click', function () {
    $('.nav-block--mob').removeClass('nav-block--open');
  });
});
//'Oferta' switch between data
$("#offer-projects").click(function(){
  if (!$(this).hasClass('offer__item--active')) {
    $(this).addClass('offer__item--active');
    $('#offer-planning').removeClass('offer__item--active');
    $('#offer-education').removeClass('offer__item--active');
  }
  $("#offer-projects-content").show(1000);
  $("#offer-planning-content").hide(50);
  $("#offer-education-content").hide(50);
});

$("#offer-planning").click(function(){
  if (!$(this).hasClass('offer__item--active')) {
    $(this).addClass('offer__item--active');
    $('#offer-projects').removeClass('offer__item--active');
    $('#offer-education').removeClass('offer__item--active');
  }
  $("#offer-planning-content").show(1000);
  $("#offer-projects-content").hide(50);
  $("#offer-education-content").hide(50);
});

$("#offer-education").click(function(){
  if (!$(this).hasClass('offer__item--active')) {
    $(this).addClass('offer__item--active');
    $('#offer-projects').removeClass('offer__item--active');
    $('#offer-planning').removeClass('offer__item--active');
  }
  $("#offer-education-content").show(1000);
  $("#offer-planning-content").hide(50);
  $("#offer-projects-content").hide(50);
});


//--------switch to single project
$(".project-link").click(function () {
  $('.projects--single').show('slow');
  $('.projects--single').css('display', 'block');
  $('.close-btn').click(function () {
    $('.projects--single').hide('slow');
    $('.projects--single').css('display', 'none');
  });
});




//NAVIGATION

var navLinks= document.querySelectorAll('.secondary-nav__link');

$(".secondary-nav li .secondary-nav__link").click(function(){
    $(this).addClass("secondary-nav__link--active");
    $(".secondary-nav li .secondary-nav__link").not(this).removeClass("secondary-nav__link--active");
});



$("#mobNavBtn").click(function(e) {
  e.preventDefault();
  $('.nav-block--mob').toggleClass('nav-block--open', 2000);
  $('.nav-block--mob li a').on('click', function () {
    $('.nav-block--mob').removeClass('nav-block--open');
  });
});


    























