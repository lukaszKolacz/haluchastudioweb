package com.lukaszkolacz.HaluchaStudioWeb.storage;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class StorageProperties {

    private String location = "haluchastudio/img";

    String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
