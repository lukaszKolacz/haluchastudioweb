package com.lukaszkolacz.HaluchaStudioWeb.repositories;

import com.lukaszkolacz.HaluchaStudioWeb.models.Project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Integer> {
    List<Project> findAll(); // iteration class
}
