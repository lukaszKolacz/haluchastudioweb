package com.lukaszkolacz.HaluchaStudioWeb.repositories;

import com.lukaszkolacz.HaluchaStudioWeb.models.Img;
import org.springframework.data.repository.CrudRepository;

public interface AddImgRepository extends CrudRepository<Img, Integer>{
}
