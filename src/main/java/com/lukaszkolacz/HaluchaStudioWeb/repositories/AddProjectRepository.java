package com.lukaszkolacz.HaluchaStudioWeb.repositories;

import com.lukaszkolacz.HaluchaStudioWeb.models.Project;
import org.springframework.data.repository.CrudRepository;

public interface AddProjectRepository extends CrudRepository<Project, Integer>{

}
