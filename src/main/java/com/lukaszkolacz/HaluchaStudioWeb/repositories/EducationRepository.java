package com.lukaszkolacz.HaluchaStudioWeb.repositories;

import com.lukaszkolacz.HaluchaStudioWeb.models.Education;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EducationRepository extends CrudRepository<Education, Integer>{
    List<Education> findAll();
}
