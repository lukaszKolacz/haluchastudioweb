package com.lukaszkolacz.HaluchaStudioWeb.repositories;


import com.lukaszkolacz.HaluchaStudioWeb.models.Information;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InformationRepository extends CrudRepository<Information, Integer> {
}
