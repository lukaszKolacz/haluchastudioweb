package com.lukaszkolacz.HaluchaStudioWeb.repositories;

import com.lukaszkolacz.HaluchaStudioWeb.models.Img;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImgRepository extends CrudRepository<Img, Integer> {
    List<Img> findAll(); // iteration class
}
