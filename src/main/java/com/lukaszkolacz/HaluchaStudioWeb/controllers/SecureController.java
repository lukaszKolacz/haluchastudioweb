package com.lukaszkolacz.HaluchaStudioWeb.controllers;

import com.lukaszkolacz.HaluchaStudioWeb.models.Project;
import com.lukaszkolacz.HaluchaStudioWeb.models.UserInfoBean;
import com.lukaszkolacz.HaluchaStudioWeb.models.forms.AddProjectForm;

import com.lukaszkolacz.HaluchaStudioWeb.repositories.AddProjectRepository;
import com.lukaszkolacz.HaluchaStudioWeb.repositories.ProjectRepository;
import com.lukaszkolacz.HaluchaStudioWeb.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.validation.Valid;
import java.util.stream.Collectors;

@Controller
public class SecureController {

    @Autowired
    UserInfoBean userInfo;
    @Autowired
    AddProjectRepository addProjectRepository;
    @Autowired
    ProjectRepository projectRepository;

    private final StorageService storageService;

    @Autowired
    public SecureController(StorageService storageService) {
        this.storageService = storageService;
    }


    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/admin")
    public String adminView() {
        return "adminView";
    }

    @GetMapping("/admin/projectForm")
    public String adminPage(Model model) {
        model.addAttribute("projectObject", new AddProjectForm());

        model.addAttribute("files", storageService.loadAll()
                        .map(path -> MvcUriComponentsBuilder.fromMethodName(FileUploadController.class, "serveFile",
                        path.getFileName().toString())
                        .build().toString())
                        .collect(Collectors.toList()));

        return "addProjectForm";
    }


    @PostMapping("/admin/projectForm")
    public String AddProjectPost(@ModelAttribute("projectObject") @Valid AddProjectForm projectForm, BindingResult result) {
        if (result.hasErrors()) {
            return "addProjectForm";
        }
        Project projectObject = new Project(projectForm);
        Project repo = addProjectRepository.save(projectObject); //Todo: sprawdzic czy da rade z tego poziomu usuwac projekt

        return "redirect:/admin/projectForm/fileupload/" + repo.getId();
    }


    @GetMapping("/403")
    @ResponseBody
    public String error403() {
        return "<center>Nie masz dostępu do tej strony!</center>";
    }


}
