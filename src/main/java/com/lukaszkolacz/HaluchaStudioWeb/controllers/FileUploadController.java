package com.lukaszkolacz.HaluchaStudioWeb.controllers;

import com.lukaszkolacz.HaluchaStudioWeb.models.Img;
import com.lukaszkolacz.HaluchaStudioWeb.models.forms.AddProjectForm;
import com.lukaszkolacz.HaluchaStudioWeb.repositories.AddImgRepository;
import com.lukaszkolacz.HaluchaStudioWeb.storage.FileSystemStorageService;
import com.lukaszkolacz.HaluchaStudioWeb.storage.StorageFileNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class FileUploadController {
    private final FileSystemStorageService storageService;

    public int projID = 1234;

    @Autowired
    AddImgRepository addImgRepository;

    List<String> radios = new ArrayList<>();


    @Autowired
    public FileUploadController(FileSystemStorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping("/admin/projectForm/fileupload/{projID}")
    public String listUploadedFiles(@PathVariable(value = "projID") String newProjId, Model model) throws IOException {

        Iterable<Img> imgList = addImgRepository.findAll();

        projID = Integer.parseInt(newProjId);

        List<String> files = storageService.getImagesById(Integer.parseInt(newProjId))
                .map(path -> MvcUriComponentsBuilder.fromMethodName(
                        FileUploadController.class, "serveFile",
                        path.getFileName().toString())
                        .build().toString())
                .collect(Collectors.toList());

        radios.clear();
        for (Path file: storageService.getImagesById(Integer.parseInt(newProjId)).collect(Collectors.toList())) {
            radios.add(file.toFile().getName());
        }

        model.addAttribute("files", files);
        model.addAttribute("radios",radios);
        model.addAttribute("selected_index",new TestModel());

        return "uploadForm";
    }

    @PostMapping("/admin/projectForm/fileupload/**")
    public String handleFileUpload(@RequestParam("file") MultipartFile[] files, RedirectAttributes redirectAttributes) {
        for (MultipartFile file : files) {
            storageService.store(file, String.valueOf(projID));
            redirectAttributes.addFlashAttribute("message", "You successfully uploaded " + file.getOriginalFilename() + "!");
        }

        return "redirect:/admin/projectForm/fileupload/" + projID;
    }

    @PostMapping("/admin/projectForm/fileupload/toImage/{imageID}")
    public String showImage(@PathVariable(value = "imageID") int imageID) {

        return "redirect:/admin/projectForm/fileupload/" + projID + "/" + imageID;
    }

    @PostMapping("/admin/projectForm/fileupload/end")
    public String handleFileUpload(@ModelAttribute("selected_index") @Valid TestModel selected_index) {


        return "redirect:/admin/";
    }

    @GetMapping("/haluchastudio/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {
        Resource file = storageService.loadAsResource(filename);

        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {

        return ResponseEntity.notFound().build();
    }
}


