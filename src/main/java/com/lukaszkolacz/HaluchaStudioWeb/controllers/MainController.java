package com.lukaszkolacz.HaluchaStudioWeb.controllers;

import com.lukaszkolacz.HaluchaStudioWeb.repositories.EducationRepository;
import com.lukaszkolacz.HaluchaStudioWeb.repositories.ImgRepository;
import com.lukaszkolacz.HaluchaStudioWeb.repositories.InformationRepository;
import com.lukaszkolacz.HaluchaStudioWeb.repositories.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class MainController {
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    InformationRepository informationRepository;
    @Autowired
    EducationRepository educationRepository;
    @Autowired
    ImgRepository imgRepository;

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("projects", projectRepository.findAll());      // Submit a list of projects
        model.addAttribute("education",educationRepository.findAll());
        model.addAttribute("about", informationRepository.findOne(1));// Submit about description
        model.addAttribute("imgs",imgRepository.findAll());
//        System.out.println(imgRepository.findOne(1));

        return "index";
    }

    @RequestMapping("/project/{projectid}")
    public String project(Model model, @PathVariable(value = "projectid") int getId) {
//        System.out.println(getId);
        model.addAttribute("project", projectRepository.findOne(getId));
        model.addAttribute("imgs",imgRepository.findAll());

        return "single_proj";
    }
}
