package com.lukaszkolacz.HaluchaStudioWeb.models;

import com.lukaszkolacz.HaluchaStudioWeb.models.forms.AddImgForm;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Img {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int projectid;
    private String url;
    private int type;
    private int profile;

    public Img() {
    }

    public Img(String projectid, String url) {
        this.projectid = Integer.parseInt(projectid);
        this.url = url;
        this.type = type;
        this.profile = profile;
    }

    public Img(AddImgForm imgForm) {
        projectid = imgForm.getProjectid();
        url = imgForm.getUrl();
        type = imgForm.getType();
        profile = imgForm.getProfile();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProjectid() {
        return projectid;
    }

    public void setProjectid(int projectid) {
        this.projectid = projectid;
    }

    public String getUrl() {
//        return "http://localhost:8080/haluchastudio/" + url;

        return "http://5.196.53.78:80/haluchastudio/" + url;

    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getProfile() {
        return profile;
    }

    public void setProfile(int profile) {
        this.profile = profile;
    }
}

