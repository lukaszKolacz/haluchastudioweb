package com.lukaszkolacz.HaluchaStudioWeb.models;

import com.lukaszkolacz.HaluchaStudioWeb.models.forms.AddProjectForm;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;


@Entity
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private Date date;
    private String title;
    private String subtitle;
    private String description;
    private String description1;
    private int itemsize;
    private int type;
    private int category;
    private int subcategory;

    public Project() {
    }

    public Project(String name, Date date, String title, String subtitle, String description, String description1, int itemsize, int type,int category, int subcategory) {
        this.name = name;
        this.date = date;
        this.title = title;
        this.subtitle = subtitle;
        this.description = description;
        this.description1 = description1;
        this.itemsize = itemsize;
        this.type = type;
        this.category = category;
        this.subcategory = subcategory;
    }

    public Project(AddProjectForm projectForm) {
        name = projectForm.getName();
        date = projectForm.getDate();
        title = projectForm.getTitle();
        subtitle = projectForm.getSubtitle();
        description = projectForm.getDescription();
        description1 = projectForm.getDescription1();
        itemsize = projectForm.getItemsize();
        type = projectForm.getType();
        subcategory = projectForm.getSubcategory();
        category = projectForm.getCategory();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }


    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription1() {
        return description1;
    }

    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    public int getItemsize() {
        return itemsize;
    }

    public void setItemsize(int itemsize) {
        this.itemsize = itemsize;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(int subcategory) {
        this.subcategory = subcategory;
    }
}
