package com.lukaszkolacz.HaluchaStudioWeb.models.forms;

public class AddImgForm {
    private int projectid;
    private String url;
    private int type;
    private int profile;

    public AddImgForm() {
    }

    private AddImgForm(Builder builder) {
        projectid = builder.projectid;
        url = builder.url;
        type = builder.type;
        profile = builder.profile;
    }

    public int getProjectid() {
        return projectid;
    }

    public void setProjectid(int projectid) {
        this.projectid = projectid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getProfile() {
        return profile;
    }

    public void setProfile(int profile) {
        this.profile = profile;
    }

    public static class Builder {
        private int projectid;
        private String url;
        private int type;
        private int profile;

        public Builder projectid(int projectid) {
            this.projectid = projectid;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder type(int type){
            this.type=type;
            return this;
        }

        public Builder profile(int profile){
            this.profile=profile;
            return this;
        }

        public AddImgForm build() {
            return new AddImgForm(this);
        }
    }
}
