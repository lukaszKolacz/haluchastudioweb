package com.lukaszkolacz.HaluchaStudioWeb.models.forms;


import java.sql.Date;


public class AddProjectForm {
    private String name;
    private Date date;
    private String title;
    private String subtitle;
    private String description;
    private String description1;
    private int itemsize;
    private int type;
    private int category;
    private int subcategory;

    public AddProjectForm() {
    }

    private AddProjectForm(Builder builder) {
        name = builder.name;
        date = builder.date;
        title = builder.title;
        subtitle = builder.subtitle;
        description = builder.description;
        itemsize = builder.itemsize;
        type = builder.type;
        category=builder.category;
        subcategory=builder.subcategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription1() {
        return description1;
    }

    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    public int getItemsize() {
        return itemsize;
    }

    public void setItemsize(int itemsize) {
        this.itemsize = itemsize;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(int subcategory) {
        this.subcategory = subcategory;
    }

    public static class Builder {
        private String name;
        private Date date;
        private String title;
        private String subtitle;
        private String description;
        private String description1;
        private int itemsize;
        private int type;
        private int category;
        private int subcategory;

        public Builder(String name) {
            this.name = name;
        }

        public Builder date(java.util.Date date) {
            this.date = new java.sql.Date(date.getTime());
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder subtitle(String subtitle) {
            this.subtitle = subtitle;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder description1(String description1) {
            this.description1 = description1;
            return this;
        }

        public Builder itemsize(int itemsize) {
            this.itemsize = itemsize;
            return this;
        }

        public Builder type(int type) {
            this.type = type;
            return this;
        }

        public Builder category(int category) {
            this.category = category;
            return this;
        }

        public Builder subcategory(int subcategory) {
            this.subcategory = subcategory;
            return this;
        }

        public AddProjectForm build() {
            return new AddProjectForm(this);
        }
    }
}
