package com.lukaszkolacz.HaluchaStudioWeb;

import com.lukaszkolacz.HaluchaStudioWeb.storage.StorageProperties;
import com.lukaszkolacz.HaluchaStudioWeb.storage.StorageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class HaluchaStudioWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(HaluchaStudioWebApplication.class, args);
	}

		@Bean
		CommandLineRunner init(StorageService storageService) {
		return (args) -> {
//			storageService.deleteAll(); //czyszczenie folderu z fotami po uruchomieniu apki
			storageService.init();
		};
	}
}
