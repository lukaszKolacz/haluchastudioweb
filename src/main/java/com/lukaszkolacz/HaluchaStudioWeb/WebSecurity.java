package com.lukaszkolacz.HaluchaStudioWeb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    @Autowired
    LoginHundler loginHundler;

    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/js/**").permitAll()
                .antMatchers("/img/**").permitAll()
                .antMatchers("/haluchastudio/**").permitAll()
                .antMatchers("/project/**").permitAll()
                .antMatchers("/login/**").permitAll()

                /*
                 TODO: Sprawdzić czy ta linijka rozwiązuje problem z logowaniem do phpmyadmin z poziomu przeglądarki (http://5.196.53.76/phpmyadmin/import.php)
                 .antMatchers("/phpmyadmin/**").permitAll()
                */

                .antMatchers("/admin/**").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .successHandler(loginHundler)
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll();

        http.exceptionHandling().accessDeniedPage("/403");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("moody").password("akademiakodu").roles("ADMIN")
                .and()
                .withUser("admin").password("test").roles("ADMIN");

    }
}
